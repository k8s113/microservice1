from fastapi import FastAPI
import random
import uuid
import redis
import socket
import structlog
from dynaconf import Dynaconf
import sys
import os
import time
from starlette.responses import FileResponse 

# Config handler
settings = Dynaconf(
    envvar_prefix="DYNACONF_",
    settings_files=["settings.yaml", ".secrets.yaml"],
)

# Logs handler
structlog.configure(logger_factory=structlog.stdlib.LoggerFactory())
log = structlog.get_logger()


# Initialize the FastAPI app
app = FastAPI()

# Initialize the Redis
r = redis.Redis(host=settings.REDIS_URL, port=settings.REDIS_PORT, db=0)


@app.get("/")
async def read_index():
    return FileResponse('frontend/index.html')

# Health check endpoint
@app.get("/health")
def health_check():
    ip = socket.gethostbyname(socket.gethostname())
    mac = ':'.join(['{:02x}'.format((uuid.getnode() >> i) & 0xff)
                   for i in range(0, 8*6, 8)][::-1])
    return {"status": "success", "host_ip": ip, "host_mac": mac}

# Endpoint to return random data


@app.get("/person")
def get_person():
    names = ["John", "Jane", "Bob", "Alice", "Charlie", "Daisy"]
    countries = ["USA", "Canada", "UK", "Australia", "Germany", "France"]
    jobs = ["Developer", "Teacher", "Nurse",
            "Engineer", "Lawyer", "Accountant"]
    person = {
        "name": random.choice(names),
        "age": random.randint(18, 70),
        "country": random.choice(countries),
        "bio": "This person is a " + random.choice(jobs),
        "job": random.choice(jobs),
        "phone_number": random.randint(1000000000, 9999999999)
    }
    log.info("Returning person data", data=person)
    try:
        # Save the data in Redis
        r.hmset("person_data", person)
        log.info("Data saved successfully", data=person)
    except Exception as e:
        log.error("Error saving data", error=e)
    return person

# Health check endpoint


@app.get("/kill")
def kill():
    log.info("Kill process")
    sys.exit(1)


# Health check endpoint
@app.get("/memfill")
def memfill():
    log.info("Filling the Memory.")
    dummy_data = bytearray(os.urandom(1024 * 1024 * 1024))


@app.get("/cpuhang")
def memfill():
    log.info("Hanging CPU")
    dummy_data = bytearray(os.urandom(1024 * 1024 * 1024))


@app.get("/sleep")
def memfill():
    log.info("Sleeping")
    time.sleep(60)
